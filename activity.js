db.rooms.insertOne({
    "name": "single",
    "accommodates": 2,
    "price": 1000,
    "description": "A simple room with all the basic necessities",
    "room_available": 10,
    "is Available" : false
})
// End single room

db.rooms.insertMany([{
    "name": "double",
    "accommodates": 3,
    "price": 2000,
    "description": "A room fit for small family going on a vacation",
    "room_available": 5,
    "is Available" : false
},
{
    "name": "queen",
    "accommodates": 4,
    "price": 4000,
    "description": "A room with a queen sized bed perfect for a small getaway",
    "room_available": 15,
    "is Available" : false
}
])
// End double and queen room


db.rooms.find({"name": "double"})
// End find method

db.rooms.updateOne(
    {"name": "queen"},
        {
            $set: {
                "room_available": 0
            }
        }
)
// End update method




db.rooms.deleteMany({

    "room_available": 0
})
//End delete method
